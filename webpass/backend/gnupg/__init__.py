import gnupg
from os import path


class GnupgBackend(object):

    def __init__(self, **kw):
        gnupg_home = kw.get(
            'gnupg_home',
            path.expanduser('~/.gnupg')
        )
        self.gnupg = gnupg.GPG(gnupghome=gnupg_home)
        self.gnupg.encoding = 'utf-8'
        self.private_keys = self.gnupg.list_keys(True)

    def login(self, identity, passphrase):
        keyid = self.get_keyid_by_uid(identity)

        if not keyid:
            return False
        return self.sign(keyid, passphrase, 'Test message')

    def get_keyid_by_uid(self, identity):
        for key in self.private_keys:
            for uid in key['uids']:
                if uid.startswith(identity):
                    return key['keyid']

    def sign(self, keyid, passphrase, message):
        return self.gnupg.sign(
            message,
            keyid=keyid,
            passphrase=passphrase
        )

    def decrypt(self, passphrase, data):
        return self.gnupg.decrypt(
            data,
            passphrase=passphrase
        )

    def decrypt_file(self, passphrase, stream):
        return str(self.gnupg.decrypt_file(
            stream,
            passphrase=passphrase
        ))
