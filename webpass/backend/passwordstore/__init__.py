from os import path
from glob import glob

class PasswordStore(object):

    def __init__(self, **kw):
        self.password_store_dir = path.expanduser(kw.get(
            'password_store_dir',
            '~/.password-store'
        ))

    def list_passwords(self):
        passwords = []
        files = glob('{base}/*/*.gpg'.format(base=self.password_store_dir))
        for index, file in enumerate(files):
            passwords.append({
                'index': index,
                'name': file.removeprefix(self.password_store_dir),
                'file': file
            })
        return passwords
