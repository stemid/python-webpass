document.addEventListener('DOMContentLoaded', () => {
  console.log('Tjena');

  $('.passwordUnlockForm').submit(function(event) {
    event.preventDefault();
    var $form = $(this),
      url = '/decrypt',
      passphrase = $form.find('input[name="passphrase"]').val(),
      file = $form.find('input[name="file"]').val(),
      index = $form.find('input[name="index"]').val();

    $.ajax({
      url: url,
      type: 'POST',
      data: JSON.stringify({
        passphrase: passphrase,
        file: file,
        index: index
      }),
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      success: (data) => {
        console.log(data.decrypted_data);
        $(`#passwordBody-${index}`).empty().append(data.decrypted_data);
        location.href = `#password-${index}`;
      }
    });
  });
});
