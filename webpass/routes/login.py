from flask import Blueprint, render_template, request, redirect, url_for
from flask import flash, session

from webpass.backend.gnupg import GnupgBackend

login_bp = Blueprint('login', __name__)

@login_bp.route('/', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')

    if request.method == 'POST':
        gpg = GnupgBackend()
        login_result = gpg.login(
            request.form['identity'],
            request.form['password']
        )
        if login_result:
            session['logged_in'] = True
            return redirect(url_for('passwords.passwords'))
        flash('Error: Identity or password is wrong')
        return redirect(url_for('login.login'))
