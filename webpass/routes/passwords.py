from json import loads
from flask import Blueprint, render_template, request, jsonify
from webpass.helpers import login_required
from webpass.backend.passwordstore import PasswordStore
from webpass.backend.gnupg import GnupgBackend

passwords_bp = Blueprint('passwords', __name__)

@passwords_bp.route('/passwords', methods=['GET', 'POST'])
@login_required
def passwords():
    ps = PasswordStore()
    return render_template('passwords.html', passwords=ps.list_passwords())

@passwords_bp.route('/decrypt', methods=['POST'])
@login_required
def decrypt():
    if not (input_data := request.get_json()):
        print(loads(request.get_data()))
        return jsonify({
            'message': 'Incorrect input format'
        }, 500)

    passphrase = input_data.get('passphrase')
    encrypted_file = input_data.get('file')

    gpg = GnupgBackend()
    try:
        decrypted_data = gpg.decrypt_file(
            passphrase=passphrase,
            stream=open(encrypted_file, 'rb')
        )
    except Exception as e:
        return jsonify({
            'message': str(e)
        }, 500)

    if not decrypted_data:
        return jsonify({
            'message': 'No data'
        }, 500)

    return jsonify({
        'message': 'Success',
        'decrypted_data': decrypted_data
    })
