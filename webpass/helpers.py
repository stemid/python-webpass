from functools import wraps
from flask import session, request, redirect, url_for


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('logged_in', False):
            return f(*args, **kwargs)
        return redirect(url_for('login.login', next=request.url))
    return decorated_function


