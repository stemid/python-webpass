import os
import atexit
from pathlib import Path
from flask import Flask
from webpass import routes

APP_NAME = 'webpass'


def create_app():
    """
    Create flask app object instance
    """
    app = Flask(__name__)
    app.secret_key = os.environ.get('FLASK_SECRET')

    register_blueprints(app)

    return app


def register_blueprints(app):
    """
    Register Flask blueprints necessary in production deployment.
    """
    app.register_blueprint(routes.login_bp)
    app.register_blueprint(routes.logout_bp)
    app.register_blueprint(routes.passwords_bp)
    return None

