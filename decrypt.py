import gnupg
import click
from os import path
from sys import exit
from getpass import getpass

@click.command()
@click.option(
    '--private-key',
    type=click.File('rb')
)
@click.option(
    '--gnupg-home',
    default=path.expanduser('~/.gnupg')
)
@click.argument(
    'input_file',
    type=click.File('rb')
)
def decrypt_file(private_key, gnupg_home, input_file):
    gpg = gnupg.GPG(gnupghome=gnupg_home)
    passphrase = getpass()
    if private_key:
        import_result = gpg.import_keys(private_key.read(), passphrase=passphrase)
    decrypted_data = gpg.decrypt_file(
        file=input_file,
        passphrase=passphrase
    )
    print(decrypted_data)


if __name__ == '__main__':
    exit(decrypt_file())
