# Simple Python web interface for pass (password manager)

This is intended to run on your workstation if you want to expose a web interface to [pass](https://www.passwordstore.org/) over your LAN so you can be lazy. Or over a VPN.

Its intended use is not as a secure product over the public internet.

Requires Python 3.9

## Scope

I thought all the current web interfaces for pass were too complex, all this project aims to do is offer a read only web interface.

## Setup & run

    python -m venv .venv
    source .venv/bin/activate
    pip install -r requirements.txt

Create an ``.env`` file with settings;

```
FLASK_APP=webpass
FLASK_ENV=production
FLASK_SECRET=predictable
GNUPG_HOME=~/.gnupg
```

Then run it;

    flask run
